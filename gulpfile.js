'use strict';

var gulp = require('gulp');


gulp.task('ng2', function() {
	//gulp.del('./public/lib/');
	var src=[
		'angular2-jwt',
		'ng2-file-upload',

		'bootstrap',
		'd3',
		'c3'
	];

	src.forEach(function(i){

		gulp.src([
		'./node_modules/'+i+'/**/*'
		],
		{'read': true}
		)
		.pipe(gulp.dest('./client/lib/'+i));

	});
});