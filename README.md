# ptracker

[![Angular 2 Style Guide](https://mgechev.github.io/angular2-style-guide/images/badge.svg)](https://angular.io/styleguide)
[![WTFPL license](https://img.shields.io/badge/license-WTFPL-brightgreen.svg)](http://www.wtfpl.net/txt/copying/)
[![wtfpl](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/txt/copying/)
[![Travis](https://img.shields.io/badge/made%20with-love-red.svg)](http://rakeen.tech)
[![version](https://img.shields.io/badge/version-0.0.1-green.svg)]()

<!--[![Dependency Status](https://david-dm.org/vyakymenko/angular2-seed-express.svg)](https://david-dm.org/vyakymenko/angular2-seed-express)
[![devDependency Status](https://david-dm.org/vyakymenko/angular2-seed-express/dev-status.svg)](https://david-dm.org/vyakymenko/angular2-seed-express#info=devDependencies)-->
<!--[![Code Climate](https://codeclimate.com/github/rakeen/ptracker/badges/gpa.svg)](https://codeclimate.com/github/rakeen/ptracker)-->

------------
![Detailed Screenshot](https://imgur.com/a/79ZeAm)  


![](https://i.imgur.com/QGF7yyH.png)  

<img src="https://i.imgur.com/P0eUHC4.png" width="500"/> 
<img src="https://i.imgur.com/zmwsCLl.png" width="500"/> 

<img src="https://i.imgur.com/jeZQAFA.png" width="500" />
<img src="https://i.imgur.com/NjiqNP3.png" width="500" />