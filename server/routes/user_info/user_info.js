var http = require('http');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('../../config.json');
var User = require('../../model/User');


exports.user_info=function user_info(user,parent){

    if(!mongoose.connection.readyState){
        parent.status(503).send("connection to DB failed!");
    }

    //console.log(req);

    User.find({"username":user},function(err,data){
        // data is an array!! :D

        var msg_err = JSON.parse('{"status":"failure","message":"Couldn\'t get user info from ptracker DB"}');

        if(err) return parent.status(500).send(msg_err);
        else{
            data=data[0];
            data.password=undefined;
            data.__v=undefined;
            data._id=undefined;
            return parent.status(200).json(data);
        }
    });
}