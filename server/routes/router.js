var express = require('express');
var jwt = require('express-jwt');
var config = require('../config.json');

var reg=require('./auth/reg');
var login=require('./auth/login');
var cfSub=require('../cf/submissionFetch');
var settings=require('../settings/settings');
var user_info=require('./user_info/user_info');


var router = express.Router(config.JWT_PRIVATE_TOKEN);

var jwtCheck = jwt({secret: config.JWT_PRIVATE_TOKEN});

router.use(function timeLog(req, res, next) {
  console.log('Time: ', new Date().toUTCString());
  next();
});




router.get('/',function(req,res){
	res.send('api working');
});

router.get('/submissions/:id',function(req,res){
	//cfSub.submissionFetch(req.params.id,res);
	cfSub.getSubmission(req.params.id,res);
});

router.post('/register',function(req,res){
	reg.register(req,res);
});


router.use('/settings/update',jwtCheck); // restricts the route with our token
router.post('/settings/update',function(req,res){
	settings.update(req,res);
});

router.post('/login',function(req,res){
	login.login(req,res);
});


router.use('/user_info/:id',jwtCheck);
router.get('/user_info/:id',function(req,res){
	user_info.user_info(req.params.id,res);
});

module.exports = router;