var http = require('http');
var mongoose = require('mongoose');

var User = require('../../model/User');


exports.register=function register(req,parent){
    user=req.body;

    if(!mongoose.connection.readyState){
        parent.status(503).send("connection to DB failed!");
    }


    User.find({"username":user.username},function(err,data){
        // data is an array!! :D

        var msg_err = JSON.parse('{"status":"failure","message":"Couldn\'t get user from ptracker DB"}');
        var msg_dup = JSON.parse('{"status":"failure","message":"Username already exists"}');
        var msg_ins = JSON.parse('{"status":"failure","message":"Couldn\'t register the user"}');
        var msg_ukn = JSON.parse('{"status":"failure","message":"Oops! Something unexpected happen!"}');
        var msg_reg = JSON.parse('{"status":"success","message":"Successfully registered"}');
        


        if(err) return parent.status(500).send(msg_err);
        else if(data.length!=0){
            return parent.status(200).json(msg_dup);
        }
        else if(data.length==0){
            new User(user).save(function(e,res){
                if(e) return parent.status(500).send(msg_ins);
                else  return parent.status(201).json(msg_reg); // 201=created
            });

        }
        else{
            return parent.status(500).json(msg_ukn);
        }
    });
}