var http = require('http');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('../../config.json');
var User = require('../../model/User');


exports.login=function login(req,parent){
    cred=req.body;

    if(!mongoose.connection.readyState){
        parent.status(503).send("connection to DB failed!");
    }

    //console.log(req);

    User.find({"username":cred.username},function(err,data){
        // data is an array!! :D

        var msg_err = JSON.parse('{"status":"failure","message":"Couldn\'t get user from ptracker DB"}');
        var msg_nou = JSON.parse('{"status":"failure","message":"Sorry, no username found"}');
        var msg_npw = JSON.parse('{"status":"failure","message":"Sorry, wrong password"}');
        var msg_ukn = JSON.parse('{"status":"failure","message":"Oops! Something unexpected happen!"}');

        var msg_ok = JSON.parse('{"status":"success","message":"Successfully authenticated"}');
        

        if(err) return parent.status(500).send(msg_err);
        else if(data.length==0){
            return parent.status(200).json(msg_nou);
        }
        else if(data.length!=0){
            data[0].comparePassword(cred.password,function(e,res){
                if(e) return parent.status(500).json(msg_ukn);
                else if(res===false) return parent.status(200).json(msg_npw);
                else if(res===true){
                    msg_ok.id_token=jwt.sign(cred,config.JWT_PRIVATE_TOKEN,{ expiresIn: "1h" });
                    msg_ok.username=data[0].username;
                    return parent.status(200).json(msg_ok);
                }
            })

        }
        else{
            return parent.status(500).json(msg_ukn);
        }
    });
}