var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require ( "mongoose" );
var app = express();


// SET ENV VAR
app.set('port', 80 || process.env.PORT || 5000 );
app.set('ip', "0.0.0.0" || process.env.IP || "127.0.0.1");

app.set('db_host',"172.17.0.3");
app.set('db_port','');


// GET SECRET
var config = require('./config.json');
var db_user = config.mongo.user;
var db_pass = config.mongo.pass;



app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use('/static', express.static(__dirname + '/public')); /// example

app.use(express.static(path.join(__dirname, '../client')));             // https://github.com/zentri/WiConnectWebApp/issues/1
app.use(express.static(path.join(__dirname, '../node_modules')));
app.use(express.static(path.join(__dirname, '../data/profile_pic')));       // profile_pic
//app.use(express.static(path.join(__dirname, '../client/homepage')));


// API endpoints
var router=require('./routes/router.js');
app.use('/api',router);

var cfSub=require('./cf/submissionFetch.js');
app.get("/sub/:id",function(req,res){
  cfSub.submissionFetch(req.params.id,res);
});


var dburi = 'mongodb://'+db_user+':'+db_pass+'@'+app.get('db_host')+'/admin';
mongoose.connect(dburi, function (err, res) {
    if (err) {
      console.error('✘ ERROR connecting to: ' + dburi + '. ' + err);
    }
    else {
      console.log('✔ MongoDB connected to: ' + dburi);
    }
});

/*
// Send the angularJS view
app.get("/", function( req, res ) {
    res.sendFile( path.join( __dirname, "../client", "index.html") );
});
*/
app.get("/500", function( req, res ) {
    res.sendFile( path.join( __dirname, "../client", "500.html") );
});


app.listen ( app.get("port"), app.get("ip"), function() {
    console.log("✔ Express server listening at %s:%d ", app.get('ip'),app.get('port'));
});
