var http = require('http');
var mongoose = require('mongoose');

var cfSubmission = require('../model/cf/submissions.js');

exports.submissionFetch=function submissionFetch(user,parent){

	var url='http://codeforces.com/api/user.status?handle='+user;
    //console.log(user);
    http.get(url,function(res){
    	var data='';
    	res.on('data',function(chunk){
    		data+=chunk;
    	});
    	res.on('end',function(chunk){
    		
    		var y=JSON.parse(data);
    		//parent.send(y.result);

            var saveResult=true;
    		y.result.forEach(function(sub){
    			var x=new cfSubmission();
    			for(p in sub){
	    			x[p]=sub[p];
	    		}
	    		
	    		x.save(function(err){
	    			if(err){
                        saveResult&=false;
                        console.error("Error saving document: ",err);
                    }
	    		});

    		});
            
            if(saveResult==true){
        		var message = JSON.parse('{"status":"success","message":"User Submissions updated"}');
    	        return parent.status(200).send(message);
            }
            else{
                var message = JSON.parse('{"status":"failure","message":"User Submissions failed"}');
                return parent.status(500).send(message);
            }

    	});
    }).on('error',function(e){
    	console.error("Error: ",e);
    });
};


exports.getSubmission=function getSubmission(user,parent){

    if(!mongoose.connection.readyState){
        parent.status(503).send("connection to DB failed!");
    }


    // nested obj query
    // ref: http://stackoverflow.com/a/26326940
    cfSubmission.find({"author.members.handle":user},function(err,data){
        var message = JSON.parse('{"status":"failure","message":"Couldn\'t get user submissions from ptracker api"}');
        if(err) parent.status(500).send(message);
        else    parent.json(data);
    });
}