var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	handle: String,
	firstName: String,
    lastName: String,
	country: String,
	city: String,
	organization: String,
	contribution: Number,
	rank: String,
	rating: Number,
	maxRank: String,
	maxRating: Number,
	lastOnlineTimeSeconds: Date,
	registrationTimeSeconds: Date
});

module.exports = mongoose.model('cfUser', Schema);


/* custom function
TaskSchema.method('isHighPriority', function() {
    if(this.priority === 1) {
        return true;
    } else {
        return false;
    }
}); 
*/