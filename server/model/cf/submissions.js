var mongoose = require('mongoose');



var problem = new mongoose.Schema({
	contestId: Number,
    index: String,
    name: String,
    type: String,
    points: Number, /*optional*/
    tags: [String]
});


var author = new mongoose.Schema({
	contestId: String,
    members: [
      {
        handle: String
      }
    ],
    participantType: String,
    ghost: Boolean,
    startTimeSeconds: Number
});

var submission = new mongoose.Schema({
	id: {type:Number, index:{unique: true,dropDups: true}},
	contestId: Number,
    creationTimeSeconds: Number,
	relativeTimeSeconds: Number,
	problem: problem,
	author: author,
	programmingLanguage: String,
	verdict: String,
	testset: String,
	passedTestCount: Number,
	timeConsumedMillis: Number,
	memoryConsumedBytes: Number
});

module.exports = mongoose.model('cfSubmission', submission);