var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10; // higher means more time


var UserSchema = new mongoose.Schema({
  username: { type: String, required: true, index: { unique: true }}, // carefull! might throw error because of the "unique"
  password: { type: String, required: true }, 

  name: String,
  email: String,
  organization: String,
  country: String,
  website: String,
  profile_pic: String,

  cf_handle: String,
  uva_handle: String
});



UserSchema.pre('save',function(next){
	var _user=this;

	if (!_user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(_user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            _user.password = hash;
            ///console.log(_user.password);
            next();
        });
    });
});



UserSchema.methods.comparePassword = function(candidatePassword,cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
        //console.log(isMatch);
    });
};


module.exports = mongoose.model('User', UserSchema);