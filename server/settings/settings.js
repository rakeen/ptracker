var http = require('http');
var mongoose = require('mongoose');
var multer = require('multer');
var path = require('path');
var fs = require('fs');

var User = require('../model/User');


var msg_err = JSON.parse('{"status":"failure","message":"Couldn\'t update user from ptracker DB"}');
var msg_upd = JSON.parse('{"status":"success","message":"Successfully updated"}');
var msg_pnm = JSON.parse('{"status":"mismatch","message":"Current Password didn\'t match"}');
var msg_fiu = JSON.parse('{"status":"failure","message":"File Upload failed"}');


exports.update = function update(req,parent){

    if(!mongoose.connection.readyState){
        return parent.status(503).send("connection to DB failed!");
    }

    // see the source code: https://github.com/expressjs/multer/blob/master/storage/disk.js
    var storage = multer.diskStorage({
        destination: 'data/profile_pic/'
    });
    var upload = multer({
        storage: storage
    }).single('profile_pic');


    upload(req,parent,function(err){
        if(err) return parent.status(500).json(msg_fiu);

        renameFile(req);
        return saveFormData(parent,req.body);
        return parent.status(200).json(msg_upd);
    });
    

}


function renameFile(req){

    if(!req.file){
        req.body.profile_pic="default.png";
        return; // if no profile_pic given
    }
    req.body.profile_pic=req.body.ls_username+path.extname(req.file.originalname);

    // ref: http://stackoverflow.com/a/32655509/4437655
    var oldFileName = path.resolve('./data/profile_pic/'+req.file.filename);
    var newFileName = path.resolve('./data/profile_pic/'+req.body.ls_username+path.extname(req.file.originalname));  

    fs.rename(oldFileName, newFileName,function(){
        console.log('File Saved!');
        console.log(oldFileName,"\n",newFileName);
    });
}

function saveFormData(parent,update_data){
    // delete if element is empty
    for(var e in update_data){
        if(update_data[e].length==0){
            delete update_data[e];
        }
    }
    //if(!update_data["profile_pic"]) delete update_data["profile_pic"];
    

    // update password
    if(update_data["new-pass"]){
        User.findOne({username: update_data.ls_username},function(err,data){

            if(err) return parent.status(500).send(msg_err);
            data.comparePassword(update_data["old-pass"],function(e,isMatch){
                if(e) return parent.status(500).send(msg_err);

                if(isMatch){
                    data.password=update_data["new-pass"];
                    data.save();


                    // since we're using mongoose we don't need to cutoff extra data. it'll update only those data that fits the schema
                    User.findOneAndUpdate({username:update_data.ls_username},update_data,{upsert: false},function(errr,d){ // IKR! :P
                        if(errr) return parent.status(500).send(msg_err);
                        else{
                            return parent.status(200).send(msg_upd);
                        }
                    });
                }
                else{
                    return parent.status(403).json(msg_pnm);
                }
            });
        });
        return;
    }



    // update without pass
    if(update_data.ls_username===undefined) return parent.status(500).send(msg_err);

    User.findOneAndUpdate({"username":update_data.ls_username},update_data,{upsert: false},function(err,data){
        if(err) return parent.status(500).send(msg_err);

        return parent.status(200).send(msg_upd);

    });
}








// deprecated :P
// upload profile pictures
function uploadFile(req,res,username){
    var upload = multer({
        dest: '/data/profile_pic/',
        limits: {
            fileSize: 5*1024*1024,
            files: 1
        },
        rename: function(fieldname,filename){
            return username;
        }
    }).single('profile_pic');

    upload(req,res,function(err){
        var msg_fiu = JSON.parse('{"status":"failure","message":"File Upload failed"}');
        if(err) return res.status(500).json(msg_fiu);
    });
}