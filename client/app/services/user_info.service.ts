import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http';

import {AuthHttp} from 'angular2-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class UserInfoService{

  constructor(public http:Http, public authHttp: AuthHttp){}

	setUserInfo(res){
		localStorage.setItem('user_email', res.email);
		localStorage.setItem('user_organization', res.organization);
		localStorage.setItem('user_country', res.country);
		localStorage.setItem('user_name', res.name);
		localStorage.setItem('user_website', res.website);
		localStorage.setItem('user_profile_pic', res.profile_pic);
	}
	get(user) {
		let header = new Headers();
		header.append('Content-Type', 'application/json');

		// handle jwt mismatch redirect
		this.authHttp.get('/api/user_info/'+user,{ headers: header })
		    .map(res => res.json()).subscribe(
										      (res)=>{
										        this.setUserInfo(res);
										      });
	}
}