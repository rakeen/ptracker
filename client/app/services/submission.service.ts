import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class SubmissionService {

  constructor(public http:Http) {
  }

  getAll(user) {
    return this.http.get('/api/submissions/'+user)
        .map(res => res.json());
  }
}