import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', loadChildren: 'app/components/dashboard/dashboard.module#DashboardModule' },
    { path: 'login', loadChildren: 'app/components/login/login.module#LoginModule' },
    { path: 'register', loadChildren: 'app/components/registration/registration.module#RegistrationModule' }
];

export const routing = RouterModule.forRoot(routes, { useHash: true });