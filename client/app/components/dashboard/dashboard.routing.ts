import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

import { ProfileComponent } from './profile/profile.component';
import { HistogramComponent } from './histogram/histogram.component';
import { HeatmapComponent } from './heatmap/heatmap.component';
import { PieComponent } from './pie/pie.component';
import { TimeseriesComponent } from './timeseries/timeseries.component';
import { SettingsComponent } from './settings/settings.component';


export const routes: Routes = [
    { path: '', component: DashboardComponent },
    { path: '', redirectTo: "dashboard" },
    { 
    	path: '', 
    	component: DashboardComponent,
    	children: [
    		{ path: 'profile', component: ProfileComponent },
    		{ path: 'histogram', component: HistogramComponent },
    		{ path: 'heatmap', component: HeatmapComponent },
    		{ path: 'pie', component: PieComponent },
    		{ path: 'timeseries', component: TimeseriesComponent },
            { path: 'settings', component: SettingsComponent }
    	]
    },
];

export const routing = RouterModule.forChild(routes);
