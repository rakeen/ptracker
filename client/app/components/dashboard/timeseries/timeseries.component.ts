import {Component} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';

import {SubmissionService} from 'app/services/submission.service';


import * as c3 from 'c3';

@Component({
    selector: 'timeseries',
    providers: [HTTP_PROVIDERS, SubmissionService],
    templateUrl: 'app/components/dashboard/timeseries/timeseries.component.html',
    styleUrls: ['c3/c3.min.css']
})

export class TimeseriesComponent {

  user = '';
  submission: any;
  data: any;
  ver_labels: any;

  constructor(public SubmissionService: SubmissionService) {
    //console.log('verdict_pie constructor go!');
  }

  generate() {
    this.SubmissionService.getAll(this.user)
      .subscribe(
      (res) => {
        this.submission = res;
        this.processData(this.user);
        this.user = '';
      }
      );
  }

  processData(user) {

    var XS = {};
    XS[user] = user + 'date';
    var u_data = new Array();
    u_data.push(user);
    var u_date = new Array();
    u_date.push(user+'date');

    var tally = new Array();
    for(let i of this.submission) {
      var creation = i.creationTimeSeconds*1000;
      var d = new Date(creation);

      var dateFormat = d3.time.format("%a %b %d %Y");
      //d = dateFormat(d);
      d = d.getFullYear();
      //d = d.toDateString();
      //console.log("->", d);

      if (typeof tally[d] === "undefined"){
        tally[d] = 1;
        u_date.push(d);
      }
      else tally[d]++;
    }

    for(let i in tally){
      u_data.push(tally[i]);
    }


    this.data = new Array();
    this.data.push(u_date,u_data);
    console.log("timeseries.ts:\t",this.data);

    this.draw(XS);
  }

  draw(XS){
    console.log(XS,this.data);

    var chart = c3.generate({
      bindto: 'div.timeseries',
      data: {
        xFormat: '%Y',
        xs: XS,
        columns: this.data,
        type: 'line'
      },
      line: {
        connectNull: true
      }
    });

  }
}