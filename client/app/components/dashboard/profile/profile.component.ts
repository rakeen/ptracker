import { Component } from '@angular/core';

@Component({
	templateUrl: 'app/components/dashboard/profile/profile.component.html',
	styleUrls: [
		'app/components/dashboard/profile/profile.component.css'
	]
})


export class ProfileComponent{
	username: string;
	name: string;
	email: string;
	website: string;
	country: string;
	organization: string;
	profile_pic: string;

	constructor(){
		this.username=localStorage.getItem('ls_username');
		this.name=localStorage.getItem('user_name');
		this.email=localStorage.getItem('user_email');
		this.website=localStorage.getItem('user_website');
		this.organization=localStorage.getItem('user_organization');
		this.country=localStorage.getItem('user_country');
		this.profile_pic=localStorage.getItem('user_profile_pic');
		
		if(this.profile_pic==null) this.profile_pic="default.png";
		console.info(this.profile_pic);
	}

}