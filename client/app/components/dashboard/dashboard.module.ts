import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";
import { ReactiveFormsModule, FormsModule }   from '@angular/forms'; // both are necessary!

import { DashboardComponent } from "./dashboard.component";
import { ProfileComponent } from "./profile/profile.component";
import { HistogramComponent } from "./histogram/histogram.component";
import { HeatmapComponent } from "./heatmap/heatmap.component";
import { PieComponent } from "./pie/pie.component";
import { TimeseriesComponent } from "./timeseries/timeseries.component";
import { SettingsComponent } from "./settings/settings.component";

import { routing } from "./dashboard.routing";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        routing
    ],
    declarations: [ DashboardComponent, ProfileComponent, HistogramComponent, HeatmapComponent, PieComponent, TimeseriesComponent, SettingsComponent ],
    bootstrap:    [ DashboardComponent ]
})
export class DashboardModule { }