import {Component, ViewEncapsulation} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';

import {SubmissionService} from 'app/services/submission.service';


import * as d3 from 'd3';
import * as CalHeatMap from 'cal-heatmap';


@Component({
    selector: 'heatmap',
    encapsulation: ViewEncapsulation.None,
    providers: [HTTP_PROVIDERS,SubmissionService],
    templateUrl: 'app/components/dashboard/heatmap/heatmap.component.html',
    styleUrls: ['cal-heatmap/cal-heatmap.css']
})

export class HeatmapComponent{

  user = '';
  submission: any;
  data: any;
  ver_labels: any;

  constructor(public submissionService: SubmissionService) {
    //this.submissionService = submissionService;
    //var x = this.submissionService.getAll("rakeen");
    
    //console.log(CalHeatMap);
  }

  generate() {
    this.submissionService.getAll(this.user)
      .subscribe(
      (res) => {
        this.submission = res;
        this.user = '';
        this.categorize();
      }
      );
  }

  // calculates the freq
  categorize() {
    this.data = {};
    this.ver_labels = new Array();

    for (let i of this.submission) {
      var creation = i.creationTimeSeconds;
      var d = creation;
      //var d = new Date(creation);
      if (d in this.data) this.data[d]++;
      else {
        this.data[d] = 1;
        this.ver_labels.push(d);
      }
    }
    //console.log(CalHeatMap);
    this.draw();
  }

  draw(){
    var prevYear = new Date();
    prevYear.setFullYear(prevYear.getFullYear() - 1); //new Date().setFullYear(new Date().getFullYear()-1)

    var cal = new CalHeatMap();
    cal.init({
      itemSelector: '#cal-heatmap',
      data: this.data,
      
      considerMissingDataAsZero: true,

      start: prevYear,
      maxDate: new Date(),
      range: 13,
      domain: 'month',
      subDomain: 'day',
      legend: [2, 4, 6, 8],
      legendColors: {
        min: "#dae289",
        max: "#3b6427",
        empty: "#ededed"
      },
      cellSize: 10
    });
  }
}