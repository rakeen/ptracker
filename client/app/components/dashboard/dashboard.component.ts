import { Component } from '@angular/core';
import { Router }   from '@angular/router';

@Component({
	templateUrl: 'app/components/dashboard/dashboard.component.html',
	styleUrls: [
		'lib/bucket-admin/assets/font-awesome/css/font-awesome.css',
		'lib/bucket-admin/css/bootstrap-reset.css',
		'lib/bucket-admin/css/style.css', 
		'lib/bucket-admin/css/style-responsive.css',
		'app/components/dashboard/dashboard.css'
	]
})

export class DashboardComponent{
	profile_pic: string;
	constructor(public router: Router){
		this.profile_pic="default.png";
		System.import('bucket-admin');
	}

	logout(){
		//localStorage.removeItem('id_token');
		//localStorage.removeItem('ls_username');
		localStorage.clear(); // clear all data
		
		this.router.navigateByUrl('/');
	}
}