import {Component} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';

import {SubmissionService} from 'app/services/submission.service';



//import * as d3 from '/lib/d3/d3.min.js';
import * as c3 from 'c3';

@Component({
    /** 
      the base url for THIS component is the url of its parent component(dashboard)  
      so if you write /  it'll refer to the app base url                               (in this case it is setup to localhost/)  
      if you write ./ it'll refer to the route of this component                       (in this case it is localhost/dashboard/verdict)
      if you write ../ it'll refer to the route of this component's parent component   (in this case it is setup to localhost/dashboard)

    */
    selector: 'verdict_pie',
    providers: [HTTP_PROVIDERS, SubmissionService],
    templateUrl: 'app/components/dashboard/pie/pie.component.html',
    styleUrls: ['c3/c3.min.css']
})

export class PieComponent {

  user = '';
  submission: any;
  data: any;
  ver_labels: any;

  constructor(public SubmissionService: SubmissionService) {
    //console.log('verdict_pie constructor go!');
  }

  generate() {
    this.SubmissionService.getAll(this.user)
      .subscribe(
      (res) => {
        this.submission = res;
        this.user = '';
        this.categorize();
      }
      );
  }

  categorize() {
    this.data = {};
    this.ver_labels = new Array();

    for (let i of this.submission) {
      var v = i.verdict;
      if (v in this.data) this.data[v]++;
      else {
        this.data[v] = 1;
        this.ver_labels.push(v);
      }
    }
    this.draw();
  }

  draw(){
    console.log(this.data);
    var chart = c3.generate({
      bindto: 'div.pie',
      data: {
        json: [this.data],
        keys: {
          value: this.ver_labels
        },
        type: 'pie'
      },
    });

  }
}