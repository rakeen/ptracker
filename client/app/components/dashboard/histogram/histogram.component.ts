import {Component} from '@angular/core';
import {SubmissionService} from 'app/services/submission.service';
import {HTTP_PROVIDERS} from '@angular/http';

import * as d3 from 'd3';

@Component({
    selector: 'histogram',
    providers: [HTTP_PROVIDERS, SubmissionService],
    templateUrl: 'app/components/dashboard/histogram/histogram.component.html',
    styleUrls: ['app/components/dashboard/histogram/histogram.component.css' ]
})

export class HistogramComponent {
  
  user = '';
  submission:any;
  data: any;

  constructor(public SubmissionService: SubmissionService) {
    //console.log('Histogram constructor go!');

    /**
    * ref: http://stackoverflow.com/a/34489991
    * this only works for System.js
    * you've to include all the necessary lib/scripts at the index.html file using traditional <script> tags
    *
    * or you could dynamically import using System.import().then()
    * or you could just import * as d3 from 'd3/d3.min'     in which case you've to import it in the index.html file
    *
    * why including <script> inside components is prohibited: http://stackoverflow.com/q/35059980
    */
    //var x = System.import('lib/d3/d3.min.js');
  }

  generate(){
    this.SubmissionService.getAll(this.user)
    .subscribe(
      (res) => {
        this.submission = res;
        this.user = '';
        this.monthly();
      }
    );
  }

  monthly() {
    this.data = new Array();
    for (let i of this.submission) {
      var creation = i.creationTimeSeconds * 1000;
      var d = new Date(creation);
      this.data.push(d);
    }
    this.d3Draw();
  }

  d3Draw(){
    var el=document.getElementsByClassName("svg-container")[0];
    if(typeof el !== "undefined") el.parentNode.removeChild(el);

    var tally = new Array();
    var formatDate = d3.time.format("%m:%Y"); //https://github.com/mbostock/d3/wiki/Time-Formatting
    this.data.forEach(function(d) {
      var k = formatDate(d);
      if (typeof tally[k] === "undefined") {
        tally[k] = 1;
      }
      else tally[k]++;
    });

    this.data = new Array();
    for(let i in tally){
      this.data.push({
        d: i,
        f: tally[i]
      });
    }

    var h = 200, w = 400;

    var x = d3.scale.ordinal().rangeRoundBands([0, w], .1);
    x.domain(this.data.map(function(d) { return d.d; }));

    var y = d3.scale.linear().range([h, 0]);
    y.domain([0, d3.max(this.data, function(d) { return d.f; })]);

    var xAxis = d3.svg.axis().scale(x).orient("bottom");
    var yAxis = d3.svg.axis().scale(y).orient("left").ticks(10,"");

    // ref: http://stackoverflow.com/a/25978286
    var svg = d3.select("div.histogram")
                .append("div")
                .classed("svg-container", true) //container class to make it responsive
                .append("svg")
                //responsive SVG needs these 2 attributes and no width and height attr
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 "+w+" "+h)
                //class to make it responsive
                .classed("svg-content-responsive", true); 

    svg.selectAll(".bar")
      .data(this.data)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) {
        return x(d.d);
      })
      .attr("width", x.rangeBand())
      .attr("y", function(d) {
        return y(d.f);
      })
      .attr("height",function(d){
        return h - y(d.f);
      });

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + h + ")")
      .call(xAxis);

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("# of problems solved");

  }
}