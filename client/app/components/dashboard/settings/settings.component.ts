import { Component, provide } from '@angular/core';
import { ReactiveFormsModule, REACTIVE_FORM_DIRECTIVES, Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http';
import { Router, RouterLink } from '@angular/router';  

import {AuthHttp, AuthConfig, tokenNotExpired, JwtHelper} from 'angular2-jwt';

import { UserInfoService } from 'app/services/user_info.service';

import { MultipartItem } from 'ng2-form/multipart-item.js';
import { MultipartUploader } from 'ng2-form/multipart-uploader.js';

@Component({
	selector: 'settings',
	templateUrl: 'app/components/dashboard/settings/settings.component.html',
	styleUrls: [],
	providers: [
		HTTP_PROVIDERS,
		provide(AuthHttp, {
			useFactory: (http) => {
				return new AuthHttp(new AuthConfig({ noJwtError: true }), http);
			},
			deps: [Http]
		}),
		UserInfoService
	],
	directives: [ REACTIVE_FORM_DIRECTIVES ]
})

export class SettingsComponent{
	private uploader: MultipartUploader = new MultipartUploader({
		url: "/api/settings/update",
		authToken: "Bearer " + localStorage.getItem('id_token')
	});
	multipartItem: MultipartItem = new MultipartItem(this.uploader);

	form: FormGroup;
	profile_pic: File;
	user_info: UserInfoService;
	constructor(private _formBuilder: FormBuilder, http: Http, authHttp: AuthHttp, public router: Router, public user_info: UserInfoService){
		this.http=http;
		this.user_info=user_info;
		//console.log(user_info);

		this.form = this._formBuilder.group({
			'name': [''],
			'email': [''],
			'country': [''],
			'organization': [''],
			'website': [''],

			'codeforces': [''],
			'uva': [''],

			'old-pass': ['', Validators.compose([this.passwordValidator])],
			'new-pass': ['',Validators.compose([this.passwordValidator])],
			'conf-pass': ['', Validators.compose([this.passwordValidator])]
		},{Validator: this.passSame});

		//console.log(this.form.controls.name,this.form.controls.name.value);
	}



	onChange(event){
		var fr = new FileReader();
		fr.readAsBinaryString(event.target.files[0]);
		fr.onload=function(){
			console.log(fr.result);
		}
		this.profile_pic=event.target.files[0];
		console.log(event.target.files[0],this.profile_pic);
	}

	selectFile($e){
		var val = $e.target;
		this.profile_pic = val.files[0];
		console.debug("Input file size: " + this.profile_pic.size);
	}

	upload(items){
		this.multipartItem.formData = new FormData();

		this.multipartItem.formData.append("profile_pic", this.profile_pic);
		this.multipartItem.formData.append("ls_username", localStorage.getItem('ls_username'));
		for(var i in items){
			console.log(i, items[i].value);
			this.multipartItem.formData.append(i, items[i].value);
		}
		this.multipartItem.callback = this.uploadCallback;
		this.multipartItem.upload();		
		this.user_info.get(localStorage.getItem('ls_username'));
	}
	uploadCallback(data){
		this.profile_pic = null; // this aint the same profile_pic in the global!
		if (data) {
			console.debug("settings updated!"); // not handling properly if jwt is not found
			console.debug(data); // handle jwt expire error
		}
		else console.error("uploading profile picture failed");
	}

	onSubmit(){
		this.upload(this.form.controls);
	}

	passwordValidator(control){
		if (!Boolean(control.value))	return null;

		var regex = /(?=.*\d).*$/; // has digit?
		if (!regex.test(control.value)) return {digitRequired: true};
		else if (control.value.length>128) return { passMax: true };
		return null;
	}

	passSame(group: ControlGroup){
		if (group.controls["new-pass"].value === group.controls["conf-pass"].value) return null;
		else return {same: true};
	}

}