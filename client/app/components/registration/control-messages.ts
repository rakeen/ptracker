import { Component, Input } from '@angular/core';

@Component({
    selector: 'control-messages',
    template: `<div *ngIf="errorMessage !== null" class="alert alert-danger">{{errorMessage}}</div>`,
})
export class ControlMessages{

    @Input()
    controlName: string;
    @Input()
    parentForm: FormGroup;

    //constructor( @Host() private _formDir: any) { console.log(_formDir); }

    // damn! this(the complexity) escelated quickly!
    get errorMessage(){
        // Find the control in the Host (Parent) form
        let c = this.parentForm.find(this.controlName);
        for (let propertyName in c.errors) {
	        // If control has a error
            if (c.errors.hasOwnProperty(propertyName) && c.touched) {
 		        // Return the appropriate error message from the Validation Service
                if (propertyName === 'required') return "This input field is required";
                else if (propertyName === 'invalidEmail') return "Invalid Email";
                else if (propertyName === 'digitRequired') return "Password should contain atleast a digit";
                else if (propertyName === 'passMax') return "Password length shouldn't be more than 128 characters!";
                else if (propertyName === 'maxlength') return "Maximum Length should be 50";
                else if (propertyName === 'minlength'){
                    if(c.errors.valueOf(propertyName).minlength && c.errors.valueOf(propertyName).minlength.requiredLength==5) return "Minimum Length should be 5";
                    else return "Minimum Length should be 4";
                }
            }
        }
        
        return null;
    }
}