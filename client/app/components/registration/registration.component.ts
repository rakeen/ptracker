import { Component } from '@angular/core';
import { Http, HTTP_PROVIDERS, Headers } from '@angular/http';
import { REACTIVE_FORM_DIRECTIVES, ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { ControlMessages } from './control-messages';

@Component({
	selector: 'registration-form',
	templateUrl: 'app/components/registration/registration.component.html',
	styleUrls: [],
	directives: [ ControlMessages,REACTIVE_FORM_DIRECTIVES ],
	providers: [ HTTP_PROVIDERS, FormBuilder ]
})


export class RegistrationComponent{
	registerForm: FormGroup;
	constructor(fb: FormBuilder, http: Http){
		this.http=http;

		this.registerForm = fb.group({
			'username': ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])], /* selector: [defaultValue,validators]*/
			'email': ['', Validators.compose([Validators.required,this.emailValidator])],
			'password': ['',Validators.compose([Validators.required,Validators.minLength(5),this.passwordValidator])]
		});

		//console.log("==>>",this.registerForm.find('email').value);
	}




	onSubmit(value: any){
		//console.log(this.registerForm.find('username').value);

		var reg_res;

		value=JSON.stringify(value); // <== important

		var headers = new Headers();
		headers.append('Content-Type', 'application/json');  
		this.http.post('./api/register', value, { headers: headers })
			.map(res => res.json())
			.subscribe(
				res => reg_res=res,
				err=> this.eerr(err),
				() => this.ff(reg_res)
			);
	}
	eerr(err){
		console.log("Error: ", err);
		window.location.href = "/500";
	}
	ff(reg_res){
		console.log('onSubmit req complete\n', reg_res);
		if (reg_res.status == "failure")	window.location.href = "/500";
		else window.location.href = "/#/login";
	}



	//http://stackoverflow.com/a/34090826/4437655
	emailValidator(control){
		// ref: http://stackoverflow.com/a/46181/4437655
		var EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		if (!EMAIL_REGEXP.test(control.value)) {
			return { invalidEmail: true };
		}
	}
	passwordValidator(control){
		var regex = /(?=.*\d).*$/; // has digit?
		if (!regex.test(control.value)) return {digitRequired: true};
		else if (control.value.length>128) return { passMax: true };
	}

}