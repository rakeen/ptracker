import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";

import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { RegistrationComponent } from "./registration.component";


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        ReactiveFormsModule,
        RouterModule.forChild([
			{ path: '', component: RegistrationComponent }
		])
    ],
    declarations: [ RegistrationComponent ],
    bootstrap:    [ RegistrationComponent ]
})
export class RegistrationModule { }