import { Component } from '@angular/core';
import { HTTP_PROVIDERS, Http, Headers } from '@angular/http';
import { REACTIVE_FORM_DIRECTIVES, ReactiveFormsModule, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router }   from '@angular/router';

import {AuthHttp, AuthConfig, tokenNotExpired, JwtHelper, AUTH_PROVIDERS } from 'angular2-jwt';
import { UserInfoService } from 'app/services/user_info.service';

@Component({
	selector: 'login',
	templateUrl: 'app/components/login/login.component.html',
	styleUrls: [],
	providers: [ HTTP_PROVIDERS, FormBuilder, AUTH_PROVIDERS, UserInfoService ],
	directives: [ REACTIVE_FORM_DIRECTIVES ]
})


export class LoginComponent{
	loginForm: FormGroup;

	//constructor(){}
	constructor(private fb: FormBuilder, http: Http,public router: Router,public user_info: UserInfoService){
		this.http=http;

		this.loginForm = fb.group({
			'username': ['', Validators.required],
			'password': ['',Validators.required]
		});
	}


	onSubmit(value: any){
		var jwt;

		value=JSON.stringify(value); // <== important

		var headers = new Headers();
		headers.append('Content-Type', 'application/json');  
		this.http.post('/api/login', value, { headers: headers })
			.map(res => res.json())
			.subscribe(
				res => jwt=res,
				err => console.log("Error: ",err),
				()  => this.authenticate(jwt)
			);
	}


	saveToken(jwt){
		if(jwt){
			localStorage.setItem('id_token',jwt);
		}
	}
	authenticate(jwt){
		if (jwt.status === "success") {
			this.saveToken(jwt.id_token);
			localStorage.setItem('ls_username', jwt.username);

			this.user_info.get(jwt.username);

			this.router.navigateByUrl('/');
		}
		else{
			console.error(jwt);
		}
	}

}