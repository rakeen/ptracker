/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {

  // map tells the System loader where to look for things
  var map = {
    'app':                        'app',

    '@angular':                   '@angular',
    'angular2-in-memory-web-api': 'angular2-in-memory-web-api',
    'rxjs':                       'rxjs',

    'd3': 'd3',
    'c3': 'c3',
    'cal-heatmap': 'cal-heatmap',
    'bucket-admin': 'lib/bucket-admin/js/bundle.min.js',
    'ng2-form': 'ng2-form/dist',
    'angular2-jwt': 'angular2-jwt'
  };

  // packages tells the System loader how to load when no filename and/or no extension
  var packages = {
    'app':                        { main: 'main.js',  defaultExtension: 'js' },
    'rxjs':                       { defaultExtension: 'js' },
    'angular2-in-memory-web-api': { main: 'index.js', defaultExtension: 'js' },

    "d3":                         { main: 'd3.min.js', format: 'amd'},
    "c3":                         { main: 'c3.min.js', format: 'amd'},
    "cal-heatmap":                { main: 'cal-heatmap.min.js', format: 'cjs', deps: ['d3']}, /* works in both cjs and amd!*/  
    "bucket-admin":               { format: 'global' },
    "angular2-jwt":               { main: 'angular2-jwt.js' }
  };

  var ngPackageNames = [
    'common',
    'compiler',
    'core',
    'forms',
    'http',
    'platform-browser',
    'platform-browser-dynamic',
    'router',
    'router-deprecated',
    'upgrade',
  ];

  // Individual files (~300 requests):
  function packIndex(pkgName) {
    packages['@angular/'+pkgName] = { main: 'index.js', defaultExtension: 'js' };
  }

  // Bundled (~40 requests):
  function packUmd(pkgName) {
    packages['@angular/'+pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
  }

  // Most environments should use UMD; some (Karma) need the individual index files
  var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;

  // Add package entries for angular packages
  ngPackageNames.forEach(setPackageConfig);

  // No umd for router yet
  packages['@angular/router'] = { main: 'index.js', defaultExtension: 'js' };

  var config = {
    map: map,
    packages: packages
  };

  System.config(config);

})(this);
